#!/bin/bash

gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq || gitlab-ctl status
gitlab-rake gitlab:backup:restore BACKUP=$1
gitlab-ctl start
gitlab-rake gitlab:check SANITIZE=true


